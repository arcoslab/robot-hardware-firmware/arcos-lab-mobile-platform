from setuptools import setup
import os
from glob import glob

package_name = 'arcos_lab_mobile_platform'

setup(
    name=package_name,
    version='0.3.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Federico Ruiz Ugalde',
    maintainer_email='memeruiz@gmail.com',
    description='ARCOS-Lab ROS2 Mobile Platform Driver',
    license='GPLv3+',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'arcos_lab_mobile_platform = arcos_lab_mobile_platform.arcos_lab_mobile_platform:main',
        ],
    },
)
