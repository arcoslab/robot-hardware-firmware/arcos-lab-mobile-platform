# Copyright (c) 2013 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from impedance_control_msgs.msg import State, Status, PosRef, RTParams
from impedance_control_msgs.msg import OCDebug
from impedance_control_msgs.srv import Modes, Cal
from geometry_msgs.msg import Twist as Twist_ros
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from rbc.pythonic_kdl import Frame, Twist
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from functools import reduce
from numpy import pi, array, dot, fmax, fmin
import logging
import numpy
from numpy.linalg import norm
from py_open_coroco import Open_coroco
import errno
import termios
from time import sleep
import yaml
from pathlib import Path

numpy.set_printoptions(
    formatter={'float': '{: 0.5f}'.format}, precision=5, suppress=True)

# STATUS1 OpenCoRoCo Flags
# Emergency stop
BINCOM_STATUS1_BREAKIN = 0
# Open loop or close loop
BINCOM_STATUS1_OPENLOOP = 1
# Motor Enabled
BINCOM_STATUS1_MOTOREN = 2
# Stopped Motor excitation off
BINCOM_STATUS1_MOTOROFF = 3
# Motor Sim (simulation of axis movement
# instead of the resolver data activated)
BINCOM_STATUS1_MOTORSIM = 4
# Est_freq overflow
BINCOM_STATUS1_EST_FREQ_OVER = 5
# Limit switch active (1: limit reached, 0: away from limit)
BINCOM_STATUS1_LIMIT_SW = 7

# Enable Motor cmd
BINCOM_CMD_FLAGS_MOTOREN = 0
# Disable Motor cmd
BINCOM_CMD_FLAGS_MOTORDIS = 1
# Motor sim cmd (use a simulated axis movement
# instead of the resolver data)
BINCOM_CMD_FLAGS_MOTORSIM = 2


numpy.seterr(divide='raise')


def nano_to_sec(now):
    return(now.nanoseconds*10**-9)


def get_flag(data, position):
    return((data & (1 << position)) > 0)


def saturate_scale(data, max_val):
    scale_factor = 1.0
    tmp = data > max_val
    for i in enumerate(tmp):
        if tmp[i]:
            print("+Saturation in ROS")
            # i is the position to saturate
            scale_factor_tmp = max_val[i]/data[i]
            if scale_factor_tmp < scale_factor:
                scale_factor = scale_factor_tmp
    # print("Scale factor: ", scale_factor)
    data *= scale_factor
    scale_factor = 1.0
    tmp = data < -max_val
    for i in enumerate(tmp):
        if tmp[i]:
            print("-Saturation in ROS")
            # i is the position to saturate
            scale_factor_tmp = -max_val[i]/data[i]
            if scale_factor_tmp < scale_factor:
                scale_factor = scale_factor_tmp
    print("Scale factor: ", scale_factor)
    data *= scale_factor
    return(data)


class PID():
    def __init__(self, p, i, d, max_act, max_cum_error):
        self.p = p
        self.i = i
        self.d = d
        self.max_act = array(max_act)
        self.cum_error_i = array([0.0]*len(self.p))
        self.last_error = array([0.0]*len(self.p))
        self.max_cum_error = array(max_cum_error)

    def reset(self):
        self.cum_error_i *= 0.0
        self.last_error *= 0.0

    def calc_ctrl(self, ref_pos, cur_pos, delta_t):
        error_p = ref_pos-cur_pos
        print("Error: ", error_p)
        self.cum_error_i += error_p*delta_t
        # saturate cumulative error i to max_cum_error
        self.cum_error_i = fmax(
            fmin(self.cum_error_i, self.max_cum_error), -self.max_cum_error)
        print("I error: ", self.cum_error_i)
        self.der_error = (error_p-self.last_error)/delta_t
        pid_act = error_p*self.p+self.cum_error_i*self.i+self.der_error*self.d
        pid_act = fmax(fmin(pid_act, self.max_act), -self.max_act)
        print("Pid Act: ", pid_act)
        return(pid_act)


class TorsoKinematics():
    def __init__(self, gear_ratio=60.0, ballscrew_ratio=0.005):
        """ Torso Kinematics

        gear_ratio: reduction ratio
        ballscrew_ratio: in meters/turn
        """
        self.gear_ratio = gear_ratio
        self.ballscrew_ratio = ballscrew_ratio/(2.0*pi)

    def calc_vfk(self, qvel):
        """ Convert motor speed to torso speed

        qvel in rads/s
        returns: meters/s"""
        return((qvel/self.gear_ratio)*self.ballscrew_ratio)

    def calc_vik(self, zvel):
        """ Convert linear torso speed to motor speed
        zvel: in meters/s
        returns: rads/s"""
        return((zvel/self.ballscrew_ratio)*self.gear_ratio)


class MecanumKinematics():
    def __init__(self, lx=1.0, ly=1.0, wheel_radius=1.0, gear_ratio=1.0):
        self.lx = lx
        self.ly = ly
        self.wheel_radius = wheel_radius
        self.gear_ratio = gear_ratio
        self.T = array([[1., -1., -(self.lx+self.ly)],
                        [1., 1., (self.lx+self.ly)],
                        [1., 1., -(self.lx+self.ly)],
                        [1., -1., (self.lx+self.ly)]
                        ])*self.gear_ratio/self.wheel_radius
        self.Tt = array([[1.0, 1.0, 1.0, 1.0],
                         [-1.0, 1.0, 1.0, -1.0],
                         [-1.0/(self.lx+self.ly), 1.0/(self.lx+self.ly), -
                          1.0/(self.lx+self.ly), 1.0/(self.lx+self.ly)]
                         ])*(self.wheel_radius/self.gear_ratio)/4.0
        self.xpos = Frame()

    def reset_xpos(self):
        self.xpos = Frame()

    def calc_vfk(self, qvel):
        """ velocity forward kinematics
        Takes joint velocities (wheels) and calculates
        cartesian velocities (platform joints)"""
        return(dot(self.Tt, qvel))

    def calc_vik(self, xvel):
        """ velocity inverse kinematics
        Takes cartesian velocities (platform joints) and calculates
        joint velocities (wheels)"""
        return(dot(self.T, xvel))

    def calc_pfk(self, qdiff):
        """ position forward kinematics
        Takes a joint angular position difference and calculates
        the new current cartesian position"""
        xdiff_temp = self.calc_vfk(qdiff)
        xdiff = Twist(
            array([xdiff_temp[0], xdiff_temp[1], 0.0, 0.0, 0.0, xdiff_temp[2]]))
        print("Cartesian increment: ", xdiff)
        xdiff_global_pos = array(dot(self.xpos.rot, xdiff[:3]))
        print("Cartesian increment global: ", xdiff_global_pos)
        xdiff_global = Twist([xdiff_global_pos[0], xdiff_global_pos[1],
                              0.0, 0.0, 0.0, xdiff[5]])
        self.xpos.add_delta(Twist(xdiff_global))
        print("Cartesian position: ", self.xpos)
        return(self.xpos)

    def get_pose(self):
        return(self.xpos)

    def test(self):
        xvels = [array([1.0, 0.0, 0.0]),
                 array([-1.0, 0.0, 0.0]),
                 array([0.0, 1.0, 0.0]),
                 array([0.0, -1.0, 0.0]),
                 array([1.0, 1.0, 0.0]),
                 array([-1.0, 1.0, 0.0]),
                 array([1.0, -1.0, 0.0]),
                 array([-1.0, -1.0, 0.0]),
                 array([0.0, 0.0, 1.0]),
                 array([0.0, 0.0, -1.0]),
                 ]
        qvels_res = [
            array([1.000,  1.000,  1.000,  1.000]),
            array([-1.000, -1.000, -1.000, -1.000]),
            array([-1.000,  1.000,  1.000, -1.000]),
            array([1.000, -1.000, -1.000,  1.000]),
            array([0.000,  2.000,  2.000,  0.000]),
            array([-2.000,  0.000,  0.000, -2.000]),
            array([2.000,  0.000,  0.000,  2.000]),
            array([0.000, -2.000, -2.000,  0.000]),
            array([-2.000, 2.000, -2.000,  2.000]),
            array([2.000, -2.000, 2.000,  -2.000]),
        ]
        print("Testing Velocity Inverse Kinematics")
        for xvel, qvel_res in zip(xvels, qvels_res):
            qvel_test = self.calc_vik(xvel)
            print(xvel, qvel_test)
            print("Diff: ", qvel_res-qvel_test)


class MobilePlatform(Node):
    def __init__(self, node_name="arcos_lab_mobile_platform"):
        super().__init__(node_name)

        self.emergency = False
        self.ocdebug_flag = True

        self.declare_parameter('num_motors', 5)
        self.num_motors = self.get_parameter(
            'num_motors').get_parameter_value().integer_value

        # simulation of each motor (by py_open_coroco) (full simulation)
        self.declare_parameter('sim', [True]*self.num_motors)
        self.sim = self.get_parameter(
            'sim').get_parameter_value().bool_array_value

        self.declare_parameter('cycle_freq', 30.0)
        self.cycle_freq = self.get_parameter(
            'cycle_freq').get_parameter_value().double_value

        self.declare_parameter('robot_name', 'aplator')
        self.robot_name = self.get_parameter(
            'robot_name').get_parameter_value().string_value

        self.declare_parameter('frame_id_basename_suffix', '_base')
        self.frame_id_basename = self.get_parameter(
            'frame_id_basename_suffix').get_parameter_value().string_value

        self.declare_parameter('frame_id_odom', 'odom')
        self.frame_id_odom = self.get_parameter(
            'frame_id_odom').get_parameter_value().string_value

        self.declare_parameter('child_frame_id_odom', 'base_link')
        self.child_frame_id_odom = self.get_parameter(
            'child_frame_id_odom').get_parameter_value().string_value

        # X, Y, angleZ, Z (torso): 4 joints
        self.declare_parameter('num_joints', 4)
        self.num_joints = self.get_parameter(
            'num_joints').get_parameter_value().integer_value

        self.motors_reconnect = [False]*self.num_motors

        self.declare_parameter('devices',
                               [
                                   "/dev/ttyOC0",
                                   "/dev/ttyOC1",
                                   "/dev/ttyOC2",
                                   "/dev/ttyOC3",
                                   "/dev/ttyOC4",
                               ])
        self.devices = self.get_parameter(
            'devices').get_parameter_value().string_array_value

        self.declare_parameter('motors',
                               ["FL",
                                "FR",
                                "RL",
                                "RR",
                                "T"])
        self.motors = self.get_parameter(
            'motors').get_parameter_value().string_array_value

        # wheel position from robot center to the side
        self.declare_parameter('lx', 0.35)
        self.lx = self.get_parameter(
            'lx').get_parameter_value().double_value

        # wheel position from robot center to the front
        self.declare_parameter('ly', 0.45)
        self.ly = self.get_parameter(
            'ly').get_parameter_value().double_value

        # wheel position from robot center to the front
        self.declare_parameter('wheel_radius', 8.0*0.0254/2.0)
        self.wheel_radius = self.get_parameter(
            'wheel_radius').get_parameter_value().double_value

        # wheels motor gear ratio (reduction)
        self.declare_parameter('wheels_gear_ratio', 8.0)
        self.wheels_gear_ratio = self.get_parameter(
            'wheels_gear_ratio').get_parameter_value().double_value

        # torso gear_ratio (reduction)
        self.declare_parameter('torso_gear_ratio', 5.0)
        self.torso_gear_ratio = self.get_parameter(
            'torso_gear_ratio').get_parameter_value().double_value

        # torso ballscrew_ratio (meters per turn)
        self.declare_parameter('torso_ballscrew_ratio', 0.005)
        self.torso_ballscrew_ratio = self.get_parameter(
            'torso_ballscrew_ratio').get_parameter_value().double_value

        # p controller
        self.declare_parameter('p', [0.4]*self.num_joints)
        self.p = self.get_parameter(
            'p').get_parameter_value().double_array_value

        # i controller
        self.declare_parameter('i', [0.0001]*self.num_joints)
        self.i = self.get_parameter(
            'i').get_parameter_value().double_array_value

        # d controller
        self.declare_parameter('d', [0.00001]*self.num_joints)
        self.d = self.get_parameter(
            'd').get_parameter_value().double_array_value

        # max pid actuation
        self.declare_parameter('max_pid_act', [70.0]*self.num_joints)
        self.max_pid_act = self.get_parameter(
            'max_pid_act').get_parameter_value().double_array_value

        # max pid cumumulative error
        self.declare_parameter('max_cum_error', [10.0]*self.num_joints)
        self.max_cum_error = self.get_parameter(
            'max_cum_error').get_parameter_value().double_array_value

        # Calibration direction, True: positive, False: negative
        self.declare_parameter('torso_cal_dir', False)
        self.torso_cal_dir = self.get_parameter(
            'torso_cal_dir').get_parameter_value().bool_value

        # Calibration movement step
        self.declare_parameter('torso_cal_step', 0.05)
        self.torso_cal_step = self.get_parameter(
            'torso_cal_step').get_parameter_value().double_value

        # Torso calibration retries
        self.declare_parameter('torso_calib_retries', 200)
        self.torso_calib_retries = self.get_parameter(
            'torso_calib_retries').get_parameter_value().integer_value

        # Torso max position
        self.declare_parameter('torso_max_pos', 0.65)
        self.torso_max_pos = self.get_parameter(
            'torso_max_pos').get_parameter_value().double_value

        # Torso min position
        self.declare_parameter('torso_min_pos', 0.0)
        self.torso_min_pos = self.get_parameter(
            'torso_min_pos').get_parameter_value().double_value

        # Torso store last position
        self.declare_parameter('torso_store_pos', True)
        self.torso_store_pos = self.get_parameter(
            'torso_store_pos').get_parameter_value().bool_value

        # Torso store last position file
        self.declare_parameter(
            'torso_store_file', ".config/arcoslab/torso.yaml")
        self.torso_store_file = self.get_parameter(
            'torso_store_file').get_parameter_value().string_value

        self.torso_store_data = {
            "torso_pos": 0.0
        }

        self.torso_store_data_loaded = False
        self.context.on_shutdown(self.store_cur_pos)

        self.cmd_flags = [0b00000000]*self.num_motors

        # OpenCoRoCo Simulation
        self.declare_parameter('oc_simulation', [False]*self.num_motors)
        self.oc_simulation = self.get_parameter(
            'oc_simulation').get_parameter_value().bool_array_value

        # Do open_coroco simulation (on the open_coroco hardware itself)
        # Simulates resolver and motor on open_coroco
        for motor in range(self.num_motors):
            if self.oc_simulation[motor]:
                self.set_level_cmd_flag(motor, BINCOM_CMD_FLAGS_MOTORSIM, True)
            else:
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTORSIM, False)

        # Control mode
        # 0: position (joint control compatible with RBC)
        # 1: velocity (cmd_vel compatible with navegation)
        self.declare_parameter('mode', 0)
        self.mode = self.get_parameter(
            'mode').get_parameter_value().integer_value

        # Timeout for receiving cmd_vel commands before setting cmd_vel
        # to zero
        self.declare_parameter('cmd_vel_timeout', 1.1)
        self.cmd_vel_timeout = self.get_parameter(
            'cmd_vel_timeout').get_parameter_value().double_value

        # Max motor speed (for all motors)
        self.declare_parameter('max_motor_speed', [
                               ((2**15)/(2**7)-1)]*self.num_motors)
        self.max_motor_speed = self.get_parameter(
            'max_motor_speed').get_parameter_value().double_array_value

        # Motor acceleration rads/s^2
        self.declare_parameter('motor_acc', [20.0, 20.0, 20.0, 20.0, 80.0])
        self.qacc = self.get_parameter(
            'motor_acc').get_parameter_value().double_array_value

        # wheel invert (for inverting wheel rotation direction)
        self.declare_parameter('wheel_invert', [-1.0, 1.0, -1.0, 1.0, -1.0])
        self.wheel_invert = self.get_parameter(
            'wheel_invert').get_parameter_value().double_array_value

        # Max cartesian speed: TODO
        # (linear platform m/s, angular platform rads/s, linear torso m/s)
        self.declare_parameter('max_cartesian_speed', [
                               0.1, 10.0*pi/180.0, 0.05])
        self.max_cartesian_speed = self.get_parameter(
            'max_cartesian_speed').get_parameter_value().double_array_value

        # sim p
        self.declare_parameter('p_sim', [10.0]*self.num_motors)
        self.p_sim = self.get_parameter(
            'p_sim').get_parameter_value().double_array_value

        # i_sim
        self.declare_parameter('i_sim', [0.001]*self.num_motors)
        self.i_sim = self.get_parameter(
            'i_sim').get_parameter_value().double_array_value

        # d_sim
        self.declare_parameter('d_sim', [0.0001]*self.num_motors)
        self.d_sim = self.get_parameter(
            'd_sim').get_parameter_value().double_array_value

        # inertia sim
        self.declare_parameter('inertia_sim', [0.01]*self.num_motors)
        self.inertia_sim = self.get_parameter(
            'inertia_sim').get_parameter_value().double_array_value

        # friction sim
        self.declare_parameter('friction_sim', [0.01]*self.num_motors)
        self.friction_sim = self.get_parameter(
            'friction_sim').get_parameter_value().double_array_value

        # stiffness sim
        self.declare_parameter('stiffness_sim', [0.1]*self.num_motors)
        self.stiffness_sim = self.get_parameter(
            'stiffness_sim').get_parameter_value().double_array_value

        self.sim_data = [y for y in zip(
            self.p_sim, self.i_sim, self.d_sim, self.inertia_sim,
            self.friction_sim, self.stiffness_sim)]

        # Publisher
        qos_profile = QoSProfile(depth=10)
        self.pub_state = self.create_publisher(
            State, self.get_name()+"/state", qos_profile)
        self.pub_status = self.create_publisher(
            Status, self.get_name()+"/status", qos_profile)
        self.pub_joint_states = self.create_publisher(
            JointState,
            self.get_name()+"/joint_states",
            qos_profile)
        self.pub_odom = self.create_publisher(
            Odometry, self.get_name()+"/odom", qos_profile)
        self.odom = Odometry()
        if self.ocdebug_flag:
            self.pub_ocdebug = self.create_publisher(
                OCDebug, self.get_name()+"/ocdebug", qos_profile)

        self.clock = self.get_clock()
        self.timer = self.create_timer(1.0/self.cycle_freq, self.my_loop)

        self.open_coroco_ctrls, self.status, self.state,\
            self.joint_states, self.oc_motor_pos_diff, self.ocdebug, self.mk, self.tk, \
            self.pid = self.create_structures_and_controllers()
        self.printi("Structures: ")
        self.printi("OpenCoRoCo Controllers")
        self.printi(len(self.open_coroco_ctrls))
        self.printi("status: ")
        self.printi(self.status)
        self.printi("state: ")
        self.printi(self.state)
        self.printi("joint_states")
        self.printi(self.joint_states)
        self.set_frame_id(self.robot_name+self.frame_id_basename,
                          self.frame_id_odom, self.child_frame_id_odom)
        self.ref_xpos = array([0.0]*self.num_joints)
        self.xpos = array([0.0]*self.num_joints)
        torso_file = Path(Path.home() / self.torso_store_file)
        if torso_file.is_file() and self.torso_store_pos:
            with open(torso_file, "r") as file:
                self.torso_store_data = yaml.safe_load(file)
            self.xpos[3] = self.torso_store_data["torso_pos"]
        self.oc_motor_speed = array([0.0]*self.num_motors)
        self.first_motor_pos_diff = 0

        # Subscribers
        self.printi(
            "Creating /posref and /rtparams topic for receiving input data")
        self.sub_robot_posref = self.create_subscription(
            PosRef,
            self.get_name()+"/posref",
            self.set_xposref,
            qos_profile)
        self.sub_robot_rtparams = self.create_subscription(
            RTParams,
            self.get_name()+"/rtparams",
            self.set_rtparams,
            qos_profile)
        print("Cmd_vel Twist subscribe topic")
        self.sub_robot_cmd_vel = self.create_subscription(
            Twist_ros, self.get_name()+"/cmd_vel", self.cmd_vel_callback,
            qos_profile)

        # Services
        # Enable service:
        self.srv = self.create_service(
            Modes, self.get_name()+'/control', self.control_srv_callback)
        # Calibrate service:
        self.cal_srv = self.create_service(
            Cal, self.get_name()+'/cal', self.cal_srv_callback)

    def cmd_vel_callback(self, data):
        # print("Cmd_vel Data: ", data)
        if self.mode == 1:
            self.ref_xvel[0] = data.linear.x
            self.ref_xvel[1] = data.linear.y
            self.ref_xvel[2] = data.angular.z
            # in case of torso, cmd_vel doesn't support torso vel cmds
            if self.num_joints == 4:
                self.ref_xvel[3] = 0.0
            self.t_cmd_vel_ros = self.clock.now()
        else:
            print("joint position mode, ignoring this command")

    def cal_srv_callback(self, request, response):
        print("Calibration callback. Request: ", request)
        failed = False
        if self.mode != 0:
            print("Control mode be set to 0. Aborting Calibration")
            failed = True
        if (any(array(self.status.enabled) != array([True]*self.num_motors))):
            print(
                "All or some motors are not enabled. All motors must be enabled before calibration. Aborting.")
            failed = True
        if self.emergency:
            print("System is in emergency stop state. Aborting")
            failed = True
        if failed:
            response.result = "Fail"
            return(response)
        else:
            print("All conditions are fine. Calibrating!")
            self.calibration_ongoing = True
            # set current pos as next pos cmd
            self.ref_xpos = array(self.xpos)
            # wait until current pos is stable
            for i in range(100):
                print()
                print("-----------------")
                print("Cal: Step 1: Stopping movement")
                print("-----------------")
                print()
                self.my_loop()
                # sleep(0.5)
                sleep(1.0/self.cycle_freq)
            # starting calibration
            for i, cal in enumerate(request.cal):
                print()
                print("-----------------")
                print("CAlibrate cmd: ", i, cal)
                if cal:
                    print("-----------------")
                    print(f"Calibrating {i} joint")
                    # sleep(0.5)
                    self.pid.reset()
                    if i < 3:
                        # resetting current position and current command
                        self.xpos[i] = 0.0
                        self.ref_xpos[i] = 0.0
                        self.mk.reset_xpos()
                    else:
                        # sleep(0.5)
                        print("-----------------")
                        print("Calibrating torso")
                        # self.xpos[i] = 0.0
                        # Setting current command to current position
                        self.ref_xpos[i] = self.xpos[i]
                        limit = False
                        retries = 10
                        tries = 0
                        while not limit:
                            # sleep(0.5)
                            print()
                            print("-----------------")
                            print("Cal: decreasing position")
                            print("-----------------")
                            if self.torso_cal_dir:
                                self.ref_xpos[3] += self.torso_cal_step
                            else:
                                self.ref_xpos[3] -= self.torso_cal_step
                            print("Ref xpos: ", self.ref_xpos)
                            for control_loops in range(100):
                                print()
                                print("-----------------")
                                print("Waiting for limit")
                                print("-----------------")
                                print("Retry: ", tries)
                                print("Ref xpos: ", self.ref_xpos)
                                self.my_loop()
                                limit = get_flag(
                                    self.status.flags1[4], BINCOM_STATUS1_LIMIT_SW)
                                print("Limit switch state: ", limit)
                                if limit:
                                    break
                                # sleep(0.5)
                                sleep(1.0/self.cycle_freq)
                            print("-----------------")
                            print("Limit switch state: ", limit)
                            if tries > self.torso_calib_retries:
                                print(
                                    "Too many calibration retries. Stopping calibration")
                                failed = True
                                break
                            else:
                                tries += 1
                        self.xpos[i] = 0.0
                        self.ref_xpos[i] = 0.0

            self.calibration_ongoing = False
            if failed:
                response.result = "Fail"
            else:
                response.result = "Success"
            return(response)

    def control_srv_callback(self, request, response):
        print("Control callback. Request: ", request)
        if request.mode != self.mode:
            print("Changing mode")
            self.mode = request.mode
            print("Setting set points to a safe value")
            # set ref_xpos to current xpos
            self.ref_xpos = self.xpos
            # set ref_xvel to cero
            self.ref_xvel = array([0.0]*self.num_joints)
        # Sending enable cmd
        # Set enable cmd_flag
        print("Setting enable cmd_flags to: ", request.enable)
        if all(request.enable) and (not any(self.status.emergencystop)):
            print("Trying to disable emergency stop")
            # TODO only do this if now breakin active
            self.emergency = False
            for motor in range(self.num_motors):
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTOREN, True)
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTORDIS, False)
        else:
            # If any request is to disable, disable all!
            for motor in range(self.num_motors):
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTOREN, False)
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTORDIS, True)
        self.update_robot_state()
        # Unset enable cmd_flag (this cmd is edge triggered)
        for motor in range(self.num_motors):
            self.set_level_cmd_flag(motor, BINCOM_CMD_FLAGS_MOTOREN, False)
            self.set_level_cmd_flag(motor, BINCOM_CMD_FLAGS_MOTORDIS, False)
        response.enabled = self.status.enabled
        print("Request: ", request.enable, response.enabled)
        if (all(array(request.enable) == array(response.enabled))
                and not self.emergency):
            response.result = "Success"
        else:
            response.result = "Fail"
        response.mode = self.status.mode
        return(response)

    def set_level_cmd_flag(self, motor, flag_num, level):
        if level:
            self.cmd_flags[motor] |= (1 << flag_num)
        else:
            self.cmd_flags[motor] &= ~(1 << flag_num)

    def printi(self, *args, **kwargs):
        self.get_logger().info(str(reduce(lambda x, y: str(x)+" "+str(y), args)))

    def set_frame_id(self, frame_id, frame_id_odom, child_frame_id_odom):
        self.joint_states.header.frame_id = frame_id
        self.odom.header.frame_id = frame_id_odom
        self.odom.child_frame_id = child_frame_id_odom

    def create_structures_and_controllers(self):
        self.printi("Creating All Structures and low level drivers")
        self.printi("Initializing State ROS msg struct")
        state = State()
        self.printi("Initializing Status ROS msg struct")
        status = Status()
        self.printi("Initializing JointState ROS msg struct")
        joint_states = JointState()
        if self.ocdebug_flag:
            self.printi("Initializing OCDebug ROS msg struct")
            ocdebug = OCDebug()
        # self.printi("Initializing SimExtEfforts ROS msg struct")
        # sim_ext_effort = SimExtEfforts()

        self.t_now = nano_to_sec(self.clock.now())
        # TODO: maybe add timestamp to status

        state.position = [0.0] * self.num_joints
        state.effort = [0.0] * self.num_joints
        state.cmdedposition = [0.0] * self.num_joints
        status.enabled = [True] * self.num_motors
        status.emergencystop = [False] * self.num_motors
        status.temp = [0.0] * self.num_motors
        status.mode = 0  # 0: position mode, 1: joint impedance mode
        status.flags1 = [0b0] * self.num_motors
        status.flags2 = [0b0] * self.num_motors
        # sim_ext_effort.effort = [0.0] * self.num_joints
        # creating zero JointState msg
        self.printi("Initializing joint_states data")
        joint_states.position.extend(
            [0.0] * (self.num_joints+4))
        joint_states.velocity.extend(
            [0.0] * (self.num_joints+4))
        joint_states.effort.extend([0.0] * (self.num_joints+4))
        for k in range(self.num_joints):
            joint_states.name.extend([
                self.robot_name+"_" + "j" + str(k)
            ])
        joint_states.name.extend([self.robot_name+"_fl"])
        joint_states.name.extend([self.robot_name+"_fr"])
        joint_states.name.extend([self.robot_name+"_rl"])
        joint_states.name.extend([self.robot_name+"_rr"])
        if self.ocdebug_flag:
            ocdebug.cmdvel.extend([0.0] * (self.num_motors))

        self.wheel_motor_pos = array([0.0]*4)

        self.printi("Creating mecanum kinematics")
        mk = MecanumKinematics(lx=self.lx, ly=self.ly,
                               wheel_radius=self.wheel_radius,
                               gear_ratio=self.wheels_gear_ratio)
        tk = TorsoKinematics(gear_ratio=self.torso_gear_ratio,
                             ballscrew_ratio=self.torso_ballscrew_ratio)
        self.ref_qvel = array([0.0]*self.num_motors)
        self.ref_xvel = array([0.0]*self.num_joints)
        self.t_cmd_vel_ros = self.clock.now()
        # xvel: (X, Y, angleZ, Z joints)
        self.xvel = array([0.0]*self.num_joints)
        # xpos: (X, Y, angleZ, Z joints)
        self.xpos = array([0.0]*self.num_joints)

        self.printi(f'Creating Motor Drivers')

        self.oc_data = array([0.0]*8)
        self.oc_data_offset = array([0.0]*8)
        # self.data=array([0.0, 0.0])
        # self.data_offset=array([0.0, 0.0])
        self.oc_angle = 0.0
        open_coroco_ctrls = []
        self.first_connect = [False]*self.num_motors
        for i, device in enumerate(self.devices):
            print("Device: ", device)
            print("Creating Open-CoRoCo controller")
            open_coroco_ctrls.append(Open_coroco(
                sim=self.sim[i], sim_data=self.sim_data[i]))
            print("Connecting to Open-CoRoCo controller")
            self.first_connect[i] = self.serial_connect(
                open_coroco_ctrls[-1], device)
        # self.oc_last_time = time.time()
        self.oc_last_rec_cmd_counter = 0
        self.oc_last_odom_counter = 0
        oc_motor_pos_diff = array([0.0]*len(self.devices))
        oc_motor_pos_diff = array([0.0]*self.num_motors)
        self.oc_motor_status = [0b00000000]*self.num_motors
        self.oc_motor_ad2s1210_fault = [0b00000000]*self.num_motors

        pid = PID(self.p, self.i, self.d, self.max_pid_act, self.max_cum_error)
        return (open_coroco_ctrls, status, state, joint_states,
                oc_motor_pos_diff, ocdebug, mk, tk, pid)

    def serial_reconnect(self, open_coroco_ctrl):
        open_coroco_ctrl.disconnect()
        open_coroco_ctrl.reconnect()

    def serial_connect(self, open_coroco_ctrl, device):
        # while True:
        try:
            result = open_coroco_ctrl.connect(
                device_name=device, baudrate=1500000)
        except OSError as e:
            print("Error: ", e.args)
            if (e.errno == errno.EIO) or (e.errno == errno.ENODEV):  # Input/output error
                print("Possibly USB disconnected")
                print("Retrying")
            else:
                print("Unhandled error: ", e.errno, " Exiting")
                print("Possibly USB disconnected")
        else:
            return(result)
        return(False)

    def update_robot_state(self):
        """Updates the robot state"""
        print()
        print()
        print("Loop start")
        # print("Node name: ", self.get_name())
        t_previous = self.t_now
        self.t_now_ros = self.clock.now()
        self.joint_states.header.stamp = self.t_now_ros.to_msg()
        self.odom.header.stamp = self.t_now_ros.to_msg()
        self.t_now = nano_to_sec(self.t_now_ros)
        delta_time = self.t_now - t_previous
        # TODO: add timestamp to SystemStatus msg

        # If last cmd_vel received more than 1.0 seconds ago, stop robot
        delta_last_cmd_vel = self.t_now - nano_to_sec(self.t_cmd_vel_ros)
        if (delta_last_cmd_vel > self.cmd_vel_timeout):
            # print("Setting ref_xvel to zero")
            self.ref_xvel *= 0.0

        # Trying to reconnect
        for motor in range(self.num_motors):
            if not self.first_connect[motor]:
                print("Trying to connect for the first time")
                self.first_connect[motor] = self.serial_connect(
                    self.open_coroco_ctrls[motor], self.devices[motor])
                self.motors_reconnect[motor] = False
            elif self.motors_reconnect[motor]:
                # Reconnect motor
                print("Trying to reconnect to motor: ", motor)
                try:
                    self.serial_reconnect(self.open_coroco_ctrls[motor])
                    # self.serial_connect(
                    #    self.open_coroco_ctrls[motor], self.devices[motor])
                except Exception as e:
                    print("Can't connect: ", e)
                else:
                    self.motors_reconnect[motor] = False

        # print("Emergency: ", self.emergency)
        if not self.emergency:
            if self.mode == 0:
                # joint position mode
                p_act = self.pid.calc_ctrl(
                    self.ref_xpos, self.xpos, delta_time)
                # p_act must be rotated to the local robot frame
                # p_act of only xy
                p_act_xy = array([p_act[0], p_act[1], 0.0])
                # p_act of only xy but rotated to local frame
                p_act_xy_local = dot(array(self.mk.get_pose().rot.T), p_act_xy)
                p_act_local = p_act_xy_local
                # p_act xy with angleZ added
                p_act_local[2] = p_act[2]
                # cartesian speed saturation:
                # xy saturation
                xy_mag = norm(p_act_local[:2])
                if xy_mag > self.max_cartesian_speed[0]:
                    print("Limit platform linear cartesian speed")
                    xy_scale = self.max_cartesian_speed[0]/xy_mag
                    p_act_local[:2] *= xy_scale
                # angleZ saturation
                if abs(p_act_local[2]) > self.max_cartesian_speed[1]:
                    print("Limit platform angular cartesian speed")
                    anglez_scale = self.max_cartesian_speed[1] / \
                        abs(p_act_local[2])
                    p_act_local[2] *= anglez_scale
                # torso saturation
                if self.num_joints == 4:
                    if abs(p_act[3]) > self.max_cartesian_speed[2]:
                        print("Limit torso linear cartesian speed")
                        ztorso_scale = self.max_cartesian_speed[2]/abs(
                            p_act[3])
                        p_act[3] *= ztorso_scale
                # Calculating wheels velocities
                self.ref_qvel[:4] = self.mk.calc_vik(p_act_local)
                # adding torso
                if self.num_joints == 4:
                    self.ref_qvel[4] = self.tk.calc_vik(p_act[3])
            elif self.mode == 1:
                # vel_cmd mode
                # no pid controller needed passing xvel directly to vik
                self.ref_qvel[:4] = self.mk.calc_vik(self.ref_xvel[:3])
                if self.num_joints == 4:
                    self.ref_qvel[4] = self.tk.calc_vik(self.ref_xvel[3])
                # Reset position PID to be sure when mode 0 enters again, it is resetted
                self.pid.reset()
        else:
            # print("Emergency. Setting ref_qvel to zero")
            self.ref_qvel *= 0.0
            # print("Setting set points to a safe value")
            # set ref_xpos to current xpos
            self.ref_xpos = self.xpos
            # set ref_xvel to cero
            self.ref_xvel = array([0.0]*self.num_joints)
            self.pid.reset()
        print("Ref qvel: ", self.ref_qvel)

        # Saturation of ref_qvel.
        # wheels ref_qvel must be all at once scaled
        # torso can be saturated separately

        # wheels
        self.ref_qvel[:4] = saturate_scale(
            self.ref_qvel[:4], array(self.max_motor_speed[:4]))

        # Torso
        if self.num_motors == 5:
            if self.ref_qvel[4] > self.max_motor_speed[4]:
                print("+Saturation in ROS torso")
                self.ref_qvel[4] = self.max_motor_speed[4]
            if self.ref_qvel[4] < -self.max_motor_speed[4]:
                print("-Saturation in ROS torso")
                self.ref_qvel[4] = -self.max_motor_speed[4]

        print("ref_qvel after saturation: ", self.ref_qvel)

        for motor, ctrl in enumerate(self.open_coroco_ctrls):
            # print("Commanding motor: ", motor)
            print(
                f"Motor[{motor}]: ref qvel: {self.ref_qvel[motor]:8.3f}, acc: {self.qacc[motor]:8.3f}, cmd_flags: {self.cmd_flags[motor]:08b}")
            if self.motors_reconnect[motor] == False:
                try:
                    ctrl.set_speed(self.wheel_invert[motor]*self.ref_qvel[motor],
                                   self.qacc[motor],
                                   cmd_flags=self.cmd_flags[motor])
                    if self.ocdebug_flag:
                        self.ocdebug.cmdvel[motor] = self.wheel_invert[motor] * \
                            self.ref_qvel[motor]
                except OSError as e:
                    print("Error: ", e.args)
                    self.motors_reconnect[motor] = True
                    self.emergency = True
                    if (e.errno == errno.EIO) or (e.errno == errno.ENODEV):
                        # Input/output error
                        print("Possibly USB disconnected")
                    else:
                        print("Unhandled error: ", e.errno)
                        print("Possibly USB disconnected")
                except termios.error as e:
                    print("Error: ", e.args)
                    self.motors_reconnect[motor] = True
                    self.emergency = True
                    print("Possibly USB disconnected")
                except AttributeError as e:
                    print("Error: ", e.args)
                    self.motors_reconnect[motor] = True
                    self.emergency = True
                    print("Possibly USB disconnected")

        for motor, ctrl in enumerate(self.open_coroco_ctrls):
            # print("Updating data from Motor: ",
            #      self.motors[motor], self.devices[motor])
            if self.motors_reconnect[motor] == False:
                try:
                    ctrl.update_data()
                except OSError as e:
                    print("Error: ", e.args)
                    self.motors_reconnect[motor] = True
                    self.emergency = True
                    if (e.errno == errno.EIO) or (e.errno == errno.ENODEV):
                        # Input/output error
                        print("Possibly USB disconnected")
                    else:
                        print("Unhandled error: ", e.errno)
                        print("Possibly USB disconnected")
                except termios.error as e:
                    print("Error: ", e.args)
                    self.motors_reconnect[motor] = True
                    self.emergency = True
                    print("Possibly USB disconnected")
                else:
                    # print(f"Motor OC current raw angle: {ctrl.get_angle()}")
                    # Ignore first motor diffs to clean them up
                    if self.first_motor_pos_diff > 100:
                        self.oc_motor_pos_diff[motor] = self.wheel_invert[motor]*ctrl.get_odometry_diff_rads(
                        )
                    else:
                        self.first_motor_pos_diff += 1
                    self.oc_motor_speed[motor] = self.wheel_invert[motor] * \
                        ctrl.get_cur_speed()
                    print(
                        f"OC cur speed: {ctrl.get_cur_speed()}, pos diff: {self.oc_motor_pos_diff[motor]:9.5f}")
                    self.oc_motor_status[motor] = ctrl.get_oc_status()
                    self.oc_motor_ad2s1210_fault[motor] = ctrl.get_ad2s1210_fault(
                    )
                    self.status.flags1[motor] = self.oc_motor_status[motor]
                    self.status.flags2[motor] = self.oc_motor_ad2s1210_fault[motor]
                    self.status.enabled[motor] = get_flag(
                        self.oc_motor_status[motor], BINCOM_STATUS1_MOTOREN)
                    self.status.emergencystop[motor] = get_flag(
                        self.oc_motor_status[motor], BINCOM_STATUS1_BREAKIN)
                    if (not self.status.enabled[motor]) or (self.status.emergencystop[motor]):
                        # Check flags and enable emergency if any motor is stopped
                        # or in any breakin is activated
                        print(
                            "Motor disabled or breakin active: activating general emergency")
                        self.emergency = True
        # print(f"OC status: ", end="")
        for motor in range(self.num_motors):
            print(f"{self.oc_motor_status[motor]:08b}", end=" ")
        print()
        # print(f"OC ad2s1210 fault: ", end="")
        for motor in range(self.num_motors):
            print(f"{self.oc_motor_ad2s1210_fault[motor]:08b}", end=" ")
        print()

        # check status flags and stop platform if there are errors

        if self.emergency:
            print("Emergency stop activated")
            # TODO:
            # Must be carefully executed to be sure is never going to leave
            # this state. Be sure there is enough chance (time?) to
            # reactivate motors and free them from previous breakins or
            # OC disables
            for motor in range(self.num_motors):
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTOREN, False)
                self.set_level_cmd_flag(
                    motor, BINCOM_CMD_FLAGS_MOTORDIS, True)
            # if there is a USB disconnection. Activate emergency
            # try to reconnect USB constantly

        # self.oc_motor_pos_diff = array([0.0, 1.0, 0.0, 1.0, 5.0])
        pose = self.mk.calc_pfk(self.oc_motor_pos_diff[:4])
        self.xpos[:2] = pose[:2, 3]
        # extract angleZ from pose.rot matrix
        self.xpos[2] = pose.to_kdl().M.GetRPY()[2]
        # for torso:
        if self.num_joints == 4:
            self.xpos[3] += self.tk.calc_vfk(self.oc_motor_pos_diff[4])
            # print("Xpos X,Y,angleZ,Z: ", self.xpos)
        else:
            pass
            # print("Xpos X,Y,angleZ: ", self.xpos)

        # calcuate xvel (X, Y, angleZ, Z joints)
        # qvel is speed of all five motors (FL, FR, RL, RR, T)
        qvel = self.oc_motor_pos_diff/delta_time
        print("Qvel (motors): ", qvel)
        self.xvel[:3] = self.mk.calc_vfk(qvel[:4])
        if self.num_joints == 4:
            self.xvel[3] = self.tk.calc_vfk(qvel[4])
            # print("Xvel X,Y,angleZ,Z: ", self.xvel)
        else:
            pass
            # print("Xvel X,Y,angleZ: ", self.xvel)

        # update odometry data
        self.odom.pose.pose = pose.to_ros2_pose()
        self.odom.twist.twist.linear.x = self.xvel[0]
        self.odom.twist.twist.linear.y = self.xvel[1]
        # odom linear z not updated from xvel because odom is only for
        # mobile platform, not torso
        self.odom.twist.twist.angular.z = self.xvel[2]

        self.status.mode = self.mode

        for joint in range(self.num_joints):
            # print("Updating ROS data for joint: ", joint)
            self.state.position[joint] = self.xpos[joint]
            self.state.cmdedposition[joint] = self.ref_xpos[joint]

        # update controller
        # self.robot_controller.update(
        #     q=self.robot_sim.get_q(),
        #     qv=self.robot_sim.get_qv(),
        #     measured_ext_torque=measured_ext_effort,
        #     delta_time=delta_time)

        # Update state from sensors
        # self.state.position = self.robot_sim.get_q().tolist()
        # print(f"State: {self.state.position}")
        # self.state.effort = measured_ext_effort.tolist()
        for k in range(self.num_joints):
            self.joint_states.position[k] = self.state.position[k]
            self.joint_states.effort[k] = self.state.effort[k]

        # wheel joint_states for wheel display
        for m in range(4):
            print("M: ", m, " Wheel motor pos: ", self.wheel_motor_pos,
                  " oc motor pos diff: ", self.oc_motor_pos_diff)
            self.wheel_motor_pos[m] += self.oc_motor_pos_diff[m] / \
                self.wheels_gear_ratio
            if self.wheel_motor_pos[m] > pi:
                self.wheel_motor_pos[m] -= 2.0*pi
            elif self.wheel_motor_pos[m] < -pi:
                self.wheel_motor_pos[m] += 2.0*pi
        print("Wheel motor pos: ", self.wheel_motor_pos)
        for l in range(self.num_joints, self.num_joints+4):
            self.joint_states.position[l] = self.wheel_motor_pos[l-self.num_joints]
            self.joint_states.effort[l] = 0.0
            self.joint_states.velocity[l] = self.oc_motor_speed[l -
                                                                self.num_joints] / self.wheels_gear_ratio
            # TODO: Add wheel speeds!!!
        if self.num_joints == 4:
            self.torso_store_data["torso_pos"] = float(self.xpos[3])

    def emergencystop(self, statuses):
        pass

    def set_xposref(self, data):
        """Sets the joints reference values"""
        if self.mode == 0:
            self.get_logger().debug(f'Commanding Robot: Joint position mode')
            if len(data.position) > 0:
                print("ROS cmded Ref position: ",
                      data.position, data.position[:3])
                if self.num_joints == 4:
                    # checking torso limits
                    if data.position[3] > self.torso_max_pos:
                        print("Torso cmd over max position. Limiting")
                        data.position[3] = self.torso_max_pos
                    elif data.position[3] < self.torso_min_pos:
                        print("Torso cmd under min position. Limiting")
                        data.position[3] = self.torso_min_pos
                self.ref_xpos = data.position
                # self.robot_controller.set_q_ref(data.position)
                # for joint, stepper_ctrl in enumerate(self.stepper_ctrls):
                #    stepper_ctrl.set_joint_ref(data.position[joint])
        else:
            self.get_logger().debug(f'vel_cmd mode, ignoring this command')

    def set_rtparams(self, data):
        """Sets the rtparams"""
        self.get_logger().debug(f'Configuring RT Params')
        # TODO: kp, velocity
        if len(data.damping) > 0:
            pass
            # self.robot_controller.set_damping(data.damping)
        if len(data.velocity) > 0:
            pass
            # self.robot_controller.set_max_velocity(data.velocity)
        if len(data.stiffness) > 0:
            pass

    def my_loop(self):
        self.update_robot_state()
        # Publish raw data
        # Publish joint_state_data
        self.pub_state.publish(self.state)
        self.pub_status.publish(self.status)
        self.pub_joint_states.publish(self.joint_states)
        if self.ocdebug_flag:
            self.pub_ocdebug.publish(self.ocdebug)
        self.pub_odom.publish(self.odom)

    def store_cur_pos(self):
        # be sure the robot is not moving anymore
        # then store last position
        # TODO: Do it for the mobile base?
        print("Storing last position to disk")
        if self.torso_store_pos:
            # Proper shutdown is not happening in rclpy:
            # https://github.com/ros2/rclpy/issues/1287
            my_path = Path(self.torso_store_file)
            Path(Path.home() / my_path.parent).mkdir(parents=True, exist_ok=True)
            with open(Path.home() / self.torso_store_file, "w") as file:
                yaml.dump(self.torso_store_data, file)


def main(args=None):
    rclpy.init(args=args)
    mobile_platform = MobilePlatform()
    try:
        rclpy.spin(mobile_platform)
    except KeyboardInterrupt:
        print("Shutdown!")
    mobile_platform.store_cur_pos()
    mobile_platform.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':
    main()
