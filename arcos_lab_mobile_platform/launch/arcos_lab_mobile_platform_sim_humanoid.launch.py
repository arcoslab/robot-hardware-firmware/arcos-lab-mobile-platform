from math import pi
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

num_joints = 4  # X,Y,angleZ, Z
num_motors = 5
node_name = "arcoslab_platform_torso"
robot_name = "aplator"
package_name = "arcos_lab_mobile_platform"


def generate_launch_description():
    conf_namespace = LaunchConfiguration("namespace", default="sim_humanoid_01")
    conf_namespace_arg = DeclareLaunchArgument(
        "namespace",
        default_value="sim_humanoid_01")
    # Run xacro first to generate complete urdf model:
    # robot_desc = subprocess.run(
    #     ["xacro", arcos-lab-ros2-humanoid-neck-driver_urdf_dir+"/urdfs"+"/arcos-lab-ros2-humanoid-neck-driver_example.xacro"], text=True, capture_output=True)

    return LaunchDescription([
        conf_namespace_arg,
        Node(
            package=package_name,
            executable=package_name,
            remappings=[
                (node_name+"/joint_states", "joint_states"),
                (node_name+"/cmd_vel", "cmd_vel"),
                (node_name+"/odom", "odom"),
            ],
            name=node_name,
            namespace=conf_namespace,
            output='screen',
            parameters=[{
                'sim': [True]*num_motors,
                'cycle_freq': 30.0,
                'robot_name': robot_name,
                'frame_id_basename_suffix': '_base',
                'frame_id_odom': 'odom',
                'child_frame_id_odom': 'base_link',
                'num_joints': num_joints,
                'devices': [
                    "/dev/ttyOC0",
                    "/dev/ttyOC1",
                    "/dev/ttyOC2",
                    "/dev/ttyOC3",
                    "/dev/ttyOC4",
                ],
                'motors': [
                    "FL",
                    "FR",
                    "RL",
                    "RR",
                    "T"
                ],
                'lx': 0.35,
                'ly': 0.45,
                'wheel_radius': 8.0*0.0254/2.0,
                'p': [0.4]*num_joints,
                'motor_acc': [20.0, 20.0, 20.0, 20.0, 20000.0],
                'max_cartesian_speed': [0.1, 10.0*pi/180.0, 1000.0],
                'oc_simulation': [False]*num_motors,
                'mode': 1,
                'cmd_vel_timeout': 1.0,
            }]
        ),
    ])
