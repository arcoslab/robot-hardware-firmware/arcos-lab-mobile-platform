import os
from math import pi
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
import subprocess

num_joints = 3  # X,Y,angleZ
num_motors = 4
node_name = "arcoslab_asimilar_platform"
robot_name = "aaplat"
package_name = "arcos_lab_mobile_platform"
platform_ns="sim_asimilar_01"

def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')
    # Run xacro first to generate complete urdf model:
    # robot_desc = subprocess.run(
    #     ["xacro", arcos-lab-ros2-humanoid-neck-driver_urdf_dir+"/urdfs"+"/arcos-lab-ros2-humanoid-neck-driver_example.xacro"], text=True, capture_output=True)

    return LaunchDescription([
        Node(
            package=package_name,
            executable=package_name,
            remappings=[
                ("/"+platform_ns+"/"+node_name+"/joint_states", "/"+platform_ns+"/joint_states"),
                ("/"+platform_ns+"/"+node_name+"/cmd_vel", "/"+platform_ns+"/cmd_vel"),
                ("/"+platform_ns+"/"+node_name+"/odom", "/"+platform_ns+"/odom"),
            ],
            name=node_name,
            namespace=platform_ns,
            output='screen',
            parameters=[{
                'sim': [True]*num_motors,
                'cycle_freq': 30.0,
                'robot_name': robot_name,
                'frame_id_basename_suffix': '_base',
                'frame_id_odom': 'odom',
                'child_frame_id_odom': 'base_link',
                'num_joints': num_joints,
                'num_motors': num_motors,
                'devices': [
                    "/dev/ttyOC0",
                    "/dev/ttyOC1",
                    "/dev/ttyOC2",
                    "/dev/ttyOC3",
                ],
                'motors': [
                    "FL",
                    "FR",
                    "RL",
                    "RR",
                ],
                'lx': 0.35,
                'ly': 0.45,
                'wheel_radius': 8.0*0.0254/2.0,
                'p': [0.4]*num_joints,
                'motor_acc': [20.0, 20.0, 20.0, 20.0],
                'max_cartesian_speed': [0.1, 10.0*pi/180.0, 1000.0],
                'oc_simulation': [False]*num_motors,
                'mode': 1,
                'cmd_vel_timeout': 1.0,
                'torso_store_pos': False,
            }]
        ),
    ])
