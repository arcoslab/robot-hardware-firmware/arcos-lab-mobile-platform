import os
from math import pi
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
import subprocess

num_joints = 4  # X,Y,angleZ, Z
num_motors = 5
node_name = "arcoslab_platform_torso"
robot_name = "aplator"
package_name = "arcos_lab_mobile_platform"
namespace = "humanoid"


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='false')
    # Run xacro first to generate complete urdf model:
    # robot_desc = subprocess.run(
    #     ["xacro", arcos-lab-ros2-humanoid-neck-driver_urdf_dir+"/urdfs"+"/arcos-lab-ros2-humanoid-neck-driver_example.xacro"], text=True, capture_output=True)

    return LaunchDescription([
        Node(
            package=package_name,
            executable=package_name,
            remappings=[
                #("/"+node_name+"/joint_states", "/joint_states"),
                ("/"+namespace+"/"+node_name+"/joint_states",
                 "/"+namespace+"/joint_states"),
                ("/"+namespace+"/"+node_name + \
                 "/cmd_vel", "/"+namespace+"/cmd_vel"),
                ("/"+namespace+"/"+node_name+"/odom", "/"+namespace+"/odom"),
            ],
            name=node_name,
            namespace=namespace,
            output='screen',
            parameters=[{
                #                'sim': [True, True, False, True, True],
                #                'sim': [True, False, True, True, True],
                'sim': [False, False, False, False, False],
                'cycle_freq': 30.0,
                'robot_name': robot_name,
                'frame_id_basename_suffix': '_base',
                'frame_id_odom': 'odom',
                'child_frame_id_odom': 'base_link',
                'num_joints': num_joints,
                'devices': [
                    "/dev/ttyOC0",
                    "/dev/ttyOC1",
                    "/dev/ttyOC2",
                    "/dev/ttyOC3",
                    "/dev/ttyOC4",
                ],
                'motors': [
                    "FL",
                    "FR",
                    "RL",
                    "RR",
                    "T"
                ],
                'lx': 0.35,
                'ly': 0.45,
                'wheel_radius': 8.0*0.0254/2.0,
                'torso_ballscrew_ratio': 0.005,
                'torso_gear_ratio': 5.0,
                'p': [0.4, 0.4, 0.4, 1.2],
                'i': [0.0001, 0.0001, 0.0001, 0.0001],
                'd': [0.00001, 0.00001, 0.00001, 0.00001],
                'max_cum_error': [10.0, 10.0, 10.0, 0.1],
                'max_pid_act': [70.0, 70.0, 70.0, 120.0],
                'max_motor_speed': [255.0, 255.0, 255.0, 255.0, 120.0],
                'motor_acc': [60.0, 60.0, 60.0, 60.0, 200.0],
                'max_cartesian_speed': [0.2, 10.0*pi/180.0, 1.0],
                'torso_max_pos': 0.675,
                'torso_min_pos': 0.0,
                'oc_simulation': [False, False, False, False, False],
                'mode': 1,
                'cmd_vel_timeout': 1.0,
            }]
        ),
    ])
