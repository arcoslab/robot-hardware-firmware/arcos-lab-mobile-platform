from setuptools import setup
import os
from glob import glob

package_name = 'arcos_lab_mobile_platform_urdf'

setup(
    name=package_name,
    version='0.3.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py')),
        (os.path.join('share', package_name)+"/urdfs", glob('urdfs/*')),
        (os.path.join('share', package_name) +
         "/meshes/convex", glob('meshes/convex/*')),
        (os.path.join('share', package_name)+"/meshes", glob('meshes/*.dae')),
        (os.path.join('share', package_name)+"/meshes", glob('meshes/*.stl')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Federico Ruiz Ugalde',
    maintainer_email='memeruiz@gmail.com',
    description='ARCOS-Lab Mobile Platform urdf files',
    license='GPLv3',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
